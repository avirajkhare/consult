import React from 'react';
import { render, screen } from '@testing-library/react';
import Hero from './hero';

test('renders Explore our GitLab solutions', () => {
  render(<Hero />);
  const linkElement = screen.getByText(/Explore our GitLab solutions/i);
  expect(linkElement).toBeInTheDocument();
});
